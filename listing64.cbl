       IDENTIFICATION DIVISION. 
       PROGRAM-ID.  LISTING-6-4 .
       AUTHOR. NARANYA BOONMEE.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  COUNTERS.
           05 HUNDRES-COUNT  PIC 99   VALUE ZEROES .
           05 TENS-COUNT     PIC 99   VALUE ZEROES .
           05 UNITS-COUNT    PIC 99   VALUE ZEROES .
       01  ODOMETER.
           05 PRN-HUNDRES    PIC 9.
           05 FILLER         PIC X VALUE "-".
           05 PRN-TENS       PIC 9.
           05 FILLER         PIC X VALUE "-".
           05 PRN-UNIT       PIC 9.
       
       PROCEDURE DIVISION .
       000-BEGIN.
           DISPLAY "Using an out-of-line Perform"
            PERFORM 001-COUNT-MILEAGE THRU 001-EXIT 
               VARYING HUNDRES-COUNT FROM 0 BY 1
               UNTIL HUNDRES-COUNT > 9
               AFTER TENS-COUNT FROM 0 BY 1 
               UNTIL TENS-COUNT > 9 
               AFTER UNITS-COUNT FROM 0 BY 1 
               UNTIL UNITS-COUNT > 9
           GOBACK 
       .

       001-COUNT-MILEAGE.
           MOVE HUNDRES-COUNT TO PRN-HUNDRES
           MOVE TENS-COUNT TO PRN-TENS 
           MOVE UNITS-COUNT TO PRN-UNIT 
           DISPLAY "Out - " ODOMETER 
       .

       001-EXIT.
           EXIT 
       .


